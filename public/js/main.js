var Process = function(id, arrival, burst, priority) {
    this.id = id;
    this.arrival = arrival;
    this.burst = burst;
    this.priority = priority;
    this.completion = 0;
    this.turnAround = 0;
    this.wait = 0;
}

var StagedProcess = function(process) {
    this.process = process;
    this.executed = 0;
}

var Bar = function(stagedProcess) {
    this.stagedProcess = stagedProcess;
    this.size = 1;
}

var Simulator = function() {
    this.processes = [];
    this.stagedProcesses = [];
    this.queue = [];
    this.graph = [];
    this.algorithm = undefined;
    this.time = 0;
    this.quantum = 0;
}

Simulator.prototype.setProcesses = function(processes) {
    this.processes = processes;
    this.stagedProcesses = [];
    this.queue = [];
    this.graph = [];
    this.time = 0;
    
    for (let i = 0 ; i < this.processes.length ; i++) {
        this.processes[i].completion = 0;
        this.processes[i].turnAround = 0;
        this.processes[i].wait = 0;
        this.stagedProcesses.push(new StagedProcess(this.processes[i]));
    }
}

Simulator.prototype.step = function() {
    if (this.algorithm == undefined)
        return false;
    
    if (this.algorithm == "RR" && this.quantum == 0)
        return false;

    // Update queue

    let stagedProcessCount = this.stagedProcesses.length;

    for (let i = 0 ; i < stagedProcessCount ; i++) {
        if (this.stagedProcesses[i].process.arrival == this.time) {
            this.queue.push(this.stagedProcesses[i]);
            this.stagedProcesses.splice(i, 1);
            stagedProcessCount--;
            i--;
        }
    }

    // Pick staged process to execute from queue

    let queueSize = this.queue.length;

    if (queueSize == 0 && stagedProcessCount == 0)
        return false;

    let stagedProcess = undefined;

    let graphSize = this.graph.length;
    let lastBarIndex = graphSize - 1;

    switch (this.algorithm) {
        case "FCFS": {
            if (queueSize > 0)
                stagedProcess = this.queue[0];
            
            break;
        }
        case "NPSJF": {
            if (graphSize > 0) {
                let hasActiveExecution = false;
                for (let i = 0 ; i < queueSize ; i++) {
                    if (this.graph[lastBarIndex].stagedProcess == this.queue[i]) {
                        stagedProcess = this.queue[i];
                        hasActiveExecution = true;
                        break;
                    }
                }

                if (hasActiveExecution == true)
                    break;
            }

            if (queueSize > 0)
                stagedProcess = this.queue[0];

            for (let i = 1 ; i < queueSize ; i++) {
                if (this.queue[i].process.burst < stagedProcess.process.burst)
                    stagedProcess = this.queue[i];
            }

            break;
        }
        case "PSJF": {
            if (queueSize > 0)
                stagedProcess = this.queue[0];

            for (let i = 1 ; i < queueSize ; i++) {
                if ((this.queue[i].process.burst - this.queue[i].executed) < (stagedProcess.process.burst - stagedProcess.executed))
                    stagedProcess = this.queue[i];
            }

            break;
        }
        case "NPP": {
            if (graphSize > 0) {
                let hasActiveExecution = false;
                for (let i = 0 ; i < queueSize ; i++) {
                    if (this.graph[lastBarIndex].stagedProcess == this.queue[i]) {
                        stagedProcess = this.queue[i];
                        hasActiveExecution = true;
                        break;
                    }
                }

                if (hasActiveExecution == true)
                    break;
            }

            if (queueSize > 0)
                stagedProcess = this.queue[0];

            for (let i = 1 ; i < queueSize ; i++) {
                if (this.queue[i].process.priority < stagedProcess.process.priority)
                    stagedProcess = this.queue[i];
            }

            break;
        }
        case "PP": {
            if (queueSize > 0)
                stagedProcess = this.queue[0];

            for (let i = 1 ; i < queueSize ; i++) {
                if (this.queue[i].process.priority < stagedProcess.process.priority)
                    stagedProcess = this.queue[i];
            }

            break;
        }
        case "RR": {
            if (queueSize > 0)
                stagedProcess = this.queue[0];

            if (graphSize > 0) {
                for (let i = 0 ; i < queueSize ; i++) {
                    if (this.graph[lastBarIndex].stagedProcess == this.queue[i]) {
                        if (this.graph[lastBarIndex].size < this.quantum)
                            stagedProcess = this.queue[i];
                        else
                            stagedProcess = this.queue[(i + 1) % queueSize];
                        
                        break;
                    }
                }
            }
            
            break;
        }
    }
    
    // Increment time

    this.time++;

    // Increment staged process execution and compute completion, turnaround and wait time

    if (stagedProcess != undefined) {
        stagedProcess.executed++;
        
        if (stagedProcess.executed == stagedProcess.process.burst) {
            stagedProcess.process.completion = this.time;
            stagedProcess.process.turnAround = stagedProcess.process.completion - stagedProcess.process.arrival;
            stagedProcess.process.wait = stagedProcess.process.turnAround - stagedProcess.process.burst;
            this.queue.splice(this.queue.indexOf(stagedProcess), 1);
            queueSize = this.queue.length;
        }
    }

    // Graph the execution

    if (graphSize > 0 && stagedProcess == this.graph[lastBarIndex].stagedProcess)
        this.graph[lastBarIndex].size++;
    else
        this.graph.push(new Bar(stagedProcess));
    
    return true;
}

Simulator.prototype.getAverageWait = function() {
    let totalWait = 0;
    let count = this.processes.length;
    
    for (let i = 0 ; i < count ; i++)
        totalWait += this.processes[i].wait;
    
    return totalWait / count;
}

Simulator.prototype.getAverageTurnAround = function() {
    let totalTurnAround = 0;
    let count = this.processes.length;

    for (let i = 0 ; i < count ; i++)
        totalTurnAround += this.processes[i].turnAround;

    return totalTurnAround / count;
}

var simulator = new Simulator();

var staged = false;
var done = false;

var processTableData = document.getElementById("process-table-data");
var time = document.getElementById("time");

var schedulingGraphHeader = document.getElementById("scheduling-graph-header");
var schedulingGraph = document.getElementById("scheduling-graph");
var schedulingGraphLabel = document.getElementById("scheduling-graph-label");

var averageWait = document.getElementById("average-wait");
var averageTurnAround = document.getElementById("average-turn-around");

document.getElementById("add-process-entry").onclick = function() {
    let count = processTableData.childElementCount;

    let newRow = document.createElement("tr");

    let newID = document.createElement("td");
    let newIDInput = document.createElement("input");
    newIDInput.setAttribute("value", count);
    newIDInput.setAttribute("readonly", "true");
    newID.appendChild(newIDInput);
    newRow.appendChild(newID);

    let newArrival = document.createElement("td");
    let newArrivalInput = document.createElement("input");
    newArrival.appendChild(newArrivalInput);
    newRow.appendChild(newArrival);

    let newBurst = document.createElement("td");
    let newBurstInput = document.createElement("input");
    newBurst.appendChild(newBurstInput);
    newRow.appendChild(newBurst);

    let newPriority = document.createElement("td");
    let newPriorityInput = document.createElement("input");
    newPriority.appendChild(newPriorityInput);
    newRow.appendChild(newPriority);

    let newCompletion = document.createElement("td");
    let newCompletionInput = document.createElement("input");
    newCompletionInput.setAttribute("value", "-");
    newCompletionInput.setAttribute("readonly", "true");
    newCompletion.appendChild(newCompletionInput);
    newRow.appendChild(newCompletion);

    let newWait = document.createElement("td");
    let newWaitInput = document.createElement("input");
    newWaitInput.setAttribute("value", "-");
    newWaitInput.setAttribute("readonly", "true");
    newWait.appendChild(newWaitInput);
    newRow.appendChild(newWait);

    let newTurnAround = document.createElement("td");
    let newTurnAroundInput = document.createElement("input");
    newTurnAroundInput.setAttribute("value", "-");
    newTurnAroundInput.setAttribute("readonly", "true");
    newTurnAround.appendChild(newTurnAroundInput);
    newRow.appendChild(newTurnAround);

    let newAction = document.createElement("td");
    
    let deleteButton = document.createElement("button");
    deleteButton.onclick = function(e) {
        e = e || window.event;
        let targ = e.target || e.srcElement;
        if (targ.nodeType == 3) targ = targ.parentNode;

        targ = targ.parentNode;

        if (targ.nodeType == 3) targ = targ.parentNode;

        targ = targ.parentNode;

        if (targ.nodeType == 3) targ = targ.parentNode;

        let nextSibling = targ.nextSibling;
        let id = targ.children[0].children[0].value;

        targ.remove();

        while (nextSibling != null) {
            nextSibling.children[0].children[0].value = id;
            id++;
            nextSibling = nextSibling.nextSibling;
        }
    };

    deleteButton.appendChild(document.createTextNode("Delete"));
    newAction.appendChild(deleteButton);

    let randomButton = document.createElement("button");

    randomButton.onclick = function(e) {
        e = e || window.event;
        let targ = e.target || e.srcElement;
        if (targ.nodeType == 3) targ = targ.parentNode;

        targ = targ.parentNode;

        if (targ.nodeType == 3) targ = targ.parentNode;

        targ = targ.parentNode;

        if (targ.nodeType == 3) targ = targ.parentNode;

        targ.children[1].children[0].value = Math.round(Math.random() * 30);
        targ.children[2].children[0].value = Math.round(Math.random() * 30) + 1;
        targ.children[3].children[0].value = Math.round(Math.random() * 30);
    }

    randomButton.appendChild(document.createTextNode("Random"));
    newAction.appendChild(randomButton);

    newRow.appendChild(newAction);

    processTableData.appendChild(newRow);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

document.getElementById("simulate-step").onclick = function() {
    if (done == true)
        return;

    if (staged == false) {
        for (let i = 0 ; i < processTableData.childElementCount ; i++) {
            let tr = processTableData.children[i];
            if (!isNumeric(tr.children[0].children[0].value) ||
                !isNumeric(tr.children[1].children[0].value) ||
                !isNumeric(tr.children[2].children[0].value) ||
                !isNumeric(tr.children[3].children[0].value) ||
                Number.parseInt(tr.children[2].children[0].value) == 0) {
                alert("Invalid inputs");
                return;
            }
        }

        let processes = [];
        for (let i = 0 ; i < processTableData.childElementCount ; i++) {
            let tr = processTableData.children[i];
            processes.push(new Process(Number.parseInt(tr.children[0].children[0].value), Number.parseInt(tr.children[1].children[0].value), Number.parseInt(tr.children[2].children[0].value), Number.parseInt(tr.children[3].children[0].value)));
        }
        simulator.setProcesses(processes);
        simulator.algorithm = "FCFS";
        staged = true;
    }

    done = !simulator.step();

    for (let i = 0 ; i < simulator.processes.length ; i++) {
        processTableData.children[i].children[4].children[0].value = simulator.processes[i].completion;
        processTableData.children[i].children[5].children[0].value = simulator.processes[i].wait;
        processTableData.children[i].children[6].children[0].value = simulator.processes[i].turnAround;
    }

    time.value = simulator.time + " ms";

    schedulingGraphHeader.setAttribute("colspan", simulator.graph.length);

    while (schedulingGraph.childElementCount > 0)
        schedulingGraph.children[0].remove();
    
    while (schedulingGraphLabel.childElementCount > 0)
        schedulingGraphLabel.children[0].remove();

    let size = 0;
    
    for (let i = 0 ; i < simulator.graph.length ; i++) {
        size += simulator.graph[i].size;

        let td = document.createElement("td");
        td.classList.add("bar");
        let id = "E";
        if (simulator.graph[i].stagedProcess != undefined)
            id = simulator.graph[i].stagedProcess.process.id;
        
        td.appendChild(document.createTextNode(id));
        schedulingGraph.appendChild(td);

        let tdLabel = document.createElement("td");
        tdLabel.appendChild(document.createTextNode(size));
        tdLabel.classList.add("text-right");
        tdLabel.classList.add("bar");
        schedulingGraphLabel.appendChild(tdLabel);
    }

    averageWait.value = simulator.getAverageWait().toFixed(2) + " ms";
    averageTurnAround.value = simulator.getAverageTurnAround().toFixed(2) + " ms";
}

document.getElementById("simulate-all").onclick = function() {
    if (done == true)
        return;

    if (staged == false) {
        for (let i = 0 ; i < processTableData.childElementCount ; i++) {
            let tr = processTableData.children[i];
            if (!isNumeric(tr.children[0].children[0].value) ||
                !isNumeric(tr.children[1].children[0].value) ||
                !isNumeric(tr.children[2].children[0].value) ||
                !isNumeric(tr.children[3].children[0].value) ||
                Number.parseInt(tr.children[2].children[0].value) == 0) {
                alert("Invalid inputs");
                return;
            }
        }

        let processes = [];
        for (let i = 0 ; i < processTableData.childElementCount ; i++) {
            let tr = processTableData.children[i];
            processes.push(new Process(Number.parseInt(tr.children[0].children[0].value), Number.parseInt(tr.children[1].children[0].value), Number.parseInt(tr.children[2].children[0].value), Number.parseInt(tr.children[3].children[0].value)));
        }
        simulator.setProcesses(processes);
        simulator.algorithm = document.getElementById("algorithm").value;
        simulator.quantum = document.getElementById("quantum").value;
        staged = true;
    }

    while (simulator.step() == true);

    for (let i = 0 ; i < simulator.processes.length ; i++) {
        processTableData.children[i].children[4].children[0].value = simulator.processes[i].completion;
        processTableData.children[i].children[5].children[0].value = simulator.processes[i].wait;
        processTableData.children[i].children[6].children[0].value = simulator.processes[i].turnAround;
    }

    time.value = simulator.time + " ms";

    schedulingGraphHeader.setAttribute("colspan", simulator.graph.length);

    while (schedulingGraph.childElementCount > 0)
        schedulingGraph.children[0].remove();
    
    while (schedulingGraphLabel.childElementCount > 0)
        schedulingGraphLabel.children[0].remove();

    let size = 0;
    
    for (let i = 0 ; i < simulator.graph.length ; i++) {
        size += simulator.graph[i].size;

        let td = document.createElement("td");
        td.classList.add("bar");
        let id = "E";
        if (simulator.graph[i].stagedProcess != undefined)
            id = simulator.graph[i].stagedProcess.process.id;
        
        td.appendChild(document.createTextNode(id));
        schedulingGraph.appendChild(td);

        let tdLabel = document.createElement("td");
        tdLabel.appendChild(document.createTextNode(size));
        tdLabel.classList.add("text-right");
        tdLabel.classList.add("bar");
        schedulingGraphLabel.appendChild(tdLabel);
    }

    averageWait.value = simulator.getAverageWait().toFixed(2) + " ms";
    averageTurnAround.value = simulator.getAverageTurnAround().toFixed(2) + " ms";

    done = true;
}

document.getElementById("clear").onclick = function() {
    while (processTableData.childElementCount > 0)
        processTableData.children[0].remove();
    
    document.getElementById("reset").onclick();
}

document.getElementById("reset").onclick = function() {
    staged = false;
    done = false;
    time.value = "0 ms";
    averageTurnAround.value = averageWait.value = "0.00 ms";
    
    while (schedulingGraph.childElementCount > 0)
        schedulingGraph.children[0].remove();
    
    while (schedulingGraphLabel.childElementCount > 0)
        schedulingGraphLabel.children[0].remove();
    
    for (let i = 0 ; i < simulator.processes.length ; i++) {
        processTableData.children[i].children[4].children[0].value = "-";
        processTableData.children[i].children[5].children[0].value = "-";
        processTableData.children[i].children[6].children[0].value = "-";
    }
}